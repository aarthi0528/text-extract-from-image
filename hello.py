from flask import Flask
import os
from flask import render_template, request, redirect,redirect, url_for
# from ocr_core import ocr_core
import pdb;
from PIL import Image
import pytesseract as pt
# from ocr_core import ocr_core
# from werkzeug import secure_filename

app = Flask(__name__)
FILE_TYPE = set(['png', 'jpg', 'jpeg'])

# function to check the file extension
def check_file_type(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in FILE_TYPE

@app.route('/')
def upload():
    return render_template("upload.html") 

@app.route('/image_to_text')
def image_to_text():
	to_text = request.args['text'].split()
	name = to_text[2]
	dob = to_text[9]
	no =  to_text[13] + to_text[14] + to_text[15]
	gender = to_text[12]
	# pdb.set_trace()
	return render_template("image_to_text.html",text =to_text, name = name, dob= dob, no = no, gender = gender) 

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

def ocr_core(filename):
	path_to_tesseract = r'C:\Program Files\Tesseract-OCR\tesseract.exe'
	pt.pytesseract.tesseract_cmd = path_to_tesseract
	text = pt.image_to_string(Image.open(filename))  # We'll use Pillow's Image class to open the image and pytesseract to detect the string in the image
	return text

# print(ocr_core('images/ocr_example_1.png'))

@app.route('/uploader', methods = ['GET','POST'])
def upload_file():
	if request.method == 'POST':
		if 'file' not in request.files:	
			return render_template('index.html', msg = 'Not Present')
		file =  request.files['file']
		if file and check_file_type(file.filename):
			# path_to_tesseract = r"C:\Program Files\Tesseract-OCR\tesseract.exe"
			# pytesseract.tesseract_cmd = path_to_tesseract
			# text = pytesseract.image_to_string(f)
			to_text = ocr_core(file)
			# f.save(secure_filename(f.filename))
			return redirect(url_for('image_to_text', text = to_text))
			# return render_template(".html", text = to_text)

if __name__ == "__main__":
	app.run(debug = True)

